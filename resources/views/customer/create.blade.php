@extends('layouts.master')
@section('breadcrumb')
    <div class="breadcrumb-text product-more">
        <a href="./home.html"><i class="fa fa-home"></i> Home</a>
        <span>Register</span>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5">
                <div class="gambar">
                    <img src="{{asset('Asset/Images/Banner/banner4.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <form method="post" action="/CustomerUser" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" class="form-control" placeholder="Enter Full Name" name="fullname" value="{{old('fullname')}}">
                    </div>
                    @error('fullname')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" placeholder="Enter Username" name="uname" value="{{old('uname')}}">
                    </div>
                    @error('uname')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="Enter Password" name="password" value="{{old('password')}}">
                    </div>
                    @error('password')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email" name="email" value="{{old('email')}}">
                    </div>
                    @error('email')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" placeholder="Enter Address" name="address" value="{{old('address')}}">
                    </div>
                    @error('address')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" placeholder="Enter Phone Number" name="phonenum" value="{{old('phonenum')}}">
                    </div>
                    @error('phonenum')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <br>
                    <button type="submit" class="btn btn-dark" style="width: 100%;">Submit</button>
                    <p>Already has an account? <a href="/login" class="btnlogin">Login</a></p>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('banner')
    <img src="{{asset('Asset/Images/Banner/banner7.jpg')}}" alt="">
@endsection
