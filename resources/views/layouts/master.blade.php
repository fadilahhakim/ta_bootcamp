<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8" />
    <meta name="description" content="Shayna Template" />
    <meta name="keywords" content="Shayna, unica, creative, html" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Matarou</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet" />

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('Template/css/bootstrap.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/font-awesome.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/themify-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/elegant-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/owl.carousel.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/nice-select.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/jquery-ui.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/slicknav.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/style.css')}}" type="text/css" />
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @include('partial.header')
    <!-- Header End -->

    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @yield('breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section End -->

    <!-- Product Shop Section Begin -->
    <section class="product-shop spad page-details">
        <div class="container">
            @yield('content')
        </div>
    </section>
    <!-- Product Shop Section End -->

    <!-- Related Products Section End -->
    <div class="related-products spad">
        <div class="container">
            @yield('content2')
        </div>
    </div>
    <!-- Related Products Section End -->

    <!-- Banner Section Begin -->
    <div class="instagram-photo">
        @yield('banner')
    </div>
    <!-- Banner Section End -->

    <!-- Footer Section Begin -->
    @include('partial.footer')
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{asset('Template/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('Template/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery.zoom.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery.dd.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('Template/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('Template/js/main.js')}}"></script>
</body>

</html>