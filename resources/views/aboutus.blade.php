@extends('layouts.master')
@section('breadcrumb')
    <div class="breadcrumb-text product-more">
        <a href="/"><i class="fa fa-home"></i> Home</a>
        <span>About Us</span>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5">
                <img src="{{asset('Asset/Images/Banner/aboutus.jpg')}}" alt="">
            </div>
            <div class="col-lg-7 col-md-7">
                <h6>Matarou Fashion</h6>
                <br>
                <p>Matarou Fashion
                For those who don't know what Matarou is. Matarou is a local fashion product that uses warm, smooth and environmentally friendly materials. Matarou follows today's fashion styles and provides styles for young people to adults.
                <br><br>

                The styles provided are from various parts of the world, we provide western styles, Asian styles, and many more. The price is also affordable according to the quality of our products.
                <br><br>

                Our delivery is fast, and we also free shipping. So don't worry about shopping with us. For delivery on the island of Java it will take from two-three working days. while outside Java it will take a maximum of ten working days.
                <br><br>

                Our collection will be updated every time there is a new collection, our collection is aesthetic and also suitable for all of you who like aesthetic fashion. So what are you waiting for?.</p>
            </div>
        </div>
    </div>
@endsection
@section('banner')
    <img src="Asset/Images/Banner/banner2.jpg" alt="">
    <img src="Asset/Images/Banner/banner3.jpg" alt="">
@endsection