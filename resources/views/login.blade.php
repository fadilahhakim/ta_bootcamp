@extends('layouts.master')
@section('breadcrumb')
    <div class="breadcrumb-text product-more">
        <a href="./home.html"><i class="fa fa-home"></i> Home</a>
        <span>Login</span>
    </div>
@endsection
@section('content')
    <div class="container">
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session('success')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if(session()->has('loginError'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{session('loginError')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-5 col-md-5">
                <div class="gambar">
                    <img src="{{asset('Asset/Images/Banner/banner6.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <form method="post" action="/login">
                    @csrf
                    <br><br>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" placeholder="Enter Username" name="userName" value="{{old('uname')}}">
                    </div>
                    @error('userName')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="Enter Password" name="password" value="{{old('password')}}">
                    </div>
                    @error('password')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <button type="submit" class="btn btn-dark" style="width: 100%;">Submit</button>
                    <p>Don't have an account? <a href="/register" class="btnregister">Register</a></p>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('banner')
    <img src="{{asset('Asset/Images/Banner/banner7.jpg')}}" alt="">
@endsection