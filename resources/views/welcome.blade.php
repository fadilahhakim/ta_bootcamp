<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8" />
    <meta name="description" content="Shayna Template" />
    <meta name="keywords" content="Shayna, unica, creative, html" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Matarou</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet" />

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('Template/css/bootstrap.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/font-awesome.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/themify-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/elegant-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/owl.carousel.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/nice-select.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/jquery-ui.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/slicknav.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('Template/css/style.css')}}" type="text/css" />
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-2">
                <div class="header__logo">
                    <a href="/"><img src="{{asset('Asset/Images/Banner/logo1.png')}}" alt=""></a>
                </div>
            </div>
            <div class="col-xl-6 col-lg-7">
                <nav class="header__menu">
                    <ul>
                        <li class="active"><a href="/">Home</a></li>
                        <li><a href="/products">Shop</a></li>
                        <li><a href="/aboutus">About Us</a></li>
                        <li><a href="./contact.html">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3">
                <div class="header__right">
                    <div class="header__right__auth">
                        <a href="/login">Login/Register</a>
                    </div>
                    <ul class="header__right__widget">
                        <li><span class="icon_search search-switch"></span></li>
                        <li><a href="/cart"><span class="icon_bag_alt"></span>
                            <div class="tip">2</div>
                        </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="canvas__open">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>
    <!-- Header End -->

    <!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="hero-items owl-carousel">
            <div class="single-hero-items set-bg" data-setbg="{{asset('Asset/Images/Banner/banner.jpg')}}">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="caption">
                                <span>Clothes,woman</span>
                                <h1>Matarou</h1>
                                <p>
                                    Matarou is a local product that is perfect for you lovers of casual looks or for those of you who usually collect casual looks. Matarou provides a variety of trendy looks for women and is also certainly up to date with current fashion developments.
                                    <br>
                                    Matarou provides a wide selection of women's fashion ranging from korean looks, western looks, with various fashion categories ranging from shorts, jackets, shirts, t-shirts. So what are you waiting for?.
                                </p>
                                <a href="/login" class="primary-btn">Shop Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-hero-items set-bg" data-setbg="{{asset('Template/img/hero-2.jpg')}}">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <span>Clothes,woman</span>
                            <h1>Matarou</h1>
                            <p>
                                Matarou is a local product that is perfect for you lovers of casual looks or for those of you who usually collect casual looks. Matarou provides a variety of trendy looks for women and is also certainly up to date with current fashion developments.
                                    <br>
                                    Matarou provides a wide selection of women's fashion ranging from korean looks, western looks, with various fashion categories ranging from shorts, jackets, shirts, t-shirts. So what are you waiting for?.
                            </p>
                            <a href="/login" class="primary-btn">Shop Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Women Banner Section Begin -->
    <section class="women-banner spad">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 mt-5">
                    <div class="product-slider owl-carousel">
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="{{asset('Template/img/mickey1.jpg')}}" alt="" />
                                <ul>
                                    <li class="w-icon active">
                                        <a href="#"><i class="icon_bag_alt"></i></a>
                                    </li>
                                    <li class="quick-view"><a href="product.html">+ Quick View</a></li>
                                </ul>
                            </div>
                            <div class="pi-text">
                                <div class="catagory-name">Coat</div>
                                <a href="#">
                                    <h5>Mickey Baggy</h5>
                                </a>
                                <div class="product-price">
                                    $14.00
                                    <span>$35.00</span>
                                </div>
                            </div>
                        </div>
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="{{asset('Template/img/products/women-2.jpg')}}" alt="" />
                                <ul>
                                    <li class="w-icon active">
                                        <a href="#"><i class="icon_bag_alt"></i></a>
                                    </li>
                                    <li class="quick-view"><a href="#">+ Quick View</a></li>
                                </ul>
                            </div>
                            <div class="pi-text">
                                <div class="catagory-name">Shoes</div>
                                <a href="#">
                                    <h5>Guangzhou sweater</h5>
                                </a>
                                <div class="product-price">
                                    $13.00
                                </div>
                            </div>
                        </div>
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="{{asset('Template/img/products/women-3.jpg')}}" alt="" />
                                <ul>
                                    <li class="w-icon active">
                                        <a href="#"><i class="icon_bag_alt"></i></a>
                                    </li>
                                    <li class="quick-view"><a href="#">+ Quick View</a></li>
                                </ul>
                            </div>
                            <div class="pi-text">
                                <div class="catagory-name">Towel</div>
                                <a href="#">
                                    <h5>Pure Pineapple</h5>
                                </a>
                                <div class="product-price">
                                    $34.00
                                </div>
                            </div>
                        </div>
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="{{asset('Template/img/products/women-4.jpg')}}" alt="" />
                                <ul>
                                    <li class="w-icon active">
                                        <a href="#"><i class="icon_bag_alt"></i></a>
                                    </li>
                                    <li class="quick-view"><a href="#">+ Quick View</a></li>
                                    <li class="w-icon">
                                        <a href="#"><i class="fa fa-random"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pi-text">
                                <div class="catagory-name">Towel</div>
                                <a href="#">
                                    <h5>Converse Shoes</h5>
                                </a>
                                <div class="product-price">
                                    $34.00
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Women Banner Section End -->

    <!-- Instagram Section Begin -->
    <div class="instagram-photo">
        <div class="insta-item set-bg" data-setbg="{{asset('Asset/Images/Banner/banner2.jpg')}}">
            <div class="inside-text">
                <i class="ti-instagram"></i>
                <h5><a href="#">matarou_shop</a></h5>
            </div>
        </div>
    </div>
    <!-- Instagram Section End -->


    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="footer-left">
                        <div class="footer-logo">
                            <a href="#"><img src="{{asset('Asset/Images/Banner/icon.jpg')}}" alt="" /></a>
                        </div>
                        <ul>
                            <li>Address: Setra Duta, Surabaya</li>
                            <li>Phone: +628 12345678</li>
                            <li>Email: hello.matarou@gmail.com</li>
                        </ul>
                        <div class="footer-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1">
                    <div class="footer-widget">
                        <h5>Information</h5>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Checkout</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Serivius</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="footer-widget">
                        <h5>My Account</h5>
                        <ul>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Shopping Cart</a></li>
                            <li><a href="#">Shop</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-reserved">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            Copyright &copy;
                            <script>
                                document.write(new Date().getFullYear());
                            </script>
                            All rights reserved | Matarou
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{asset('Template/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('Template/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery.zoom.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery.dd.min.js')}}"></script>
    <script src="{{asset('Template/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('Template/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('Template/js/main.js')}}"></script>
</body>

</html>