<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\CustomerController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/productdetail',[Controller::class,"productdetail"]);
Route::get('/cart',[Controller::class,"carts"]);
Route::get('/register',[RegisterController::class,"pindahhalregist"]);
Route::get('/login',[LoginController::class,"pindahlogin"]);
Route::get('/aboutus',[Controller::class,"aboutus"]);
Route::get('/products',[Controller::class,"products"]);

//register customer
Route::resource('CustomerUser','CustomerController');

//login customer
Route::post('/login',[LoginController::class,"auth"]);