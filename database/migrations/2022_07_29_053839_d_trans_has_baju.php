<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DTransHasBaju extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_trans_has_baju', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('baju_id');
            $table->foreign('baju_id')
                    ->references('id')
                    ->on('baju')
                    ->onDelete('cascade');
            $table->unsignedBigInteger('d_trans_id');
            $table->foreign('d_trans_id')
                    ->references('id')
                    ->on('d_trans')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_trans_has_baju');
    }
}
