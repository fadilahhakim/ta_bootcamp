<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DTrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_trans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('NomorNota');
            $table->integer('quantity');
            $table->integer('string');
            $table->unsignedBigInteger('baju_id');
            $table->foreign('baju_id')
                    ->references('id')
                    ->on('baju')
                    ->onDelete('cascade');
            $table->unsignedBigInteger('h_trans_id');
            $table->foreign('h_trans_id')
                    ->references('id')
                    ->on('h_trans')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_trans');
    }
}
