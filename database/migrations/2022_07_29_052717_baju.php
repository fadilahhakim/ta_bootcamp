<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Baju extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baju', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('deskripsi');
            $table->integer('stok');
            $table->integer('harga');
            $table->unsignedBigInteger('kategori_baju_id');
            $table->foreign('kategori_baju_id')
                    ->references('id')
                    ->on('kategori_baju')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baju');
    }
}
