<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerUser extends Model
{
    protected $table = 'user';
    protected $fillable = ['fullNAme','userName','password','email','alamat','noTelp'];
}
