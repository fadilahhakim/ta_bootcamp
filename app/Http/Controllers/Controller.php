<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function productdetail()
    {
        return view('ProductDetail');
    }
    public function carts()
    {
        return view('cart');
    }
    public function aboutus(){
        return view('aboutus');
    }
    public function products()
    {
        return view('products');
    }
}
